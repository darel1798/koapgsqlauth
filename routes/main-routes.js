const Router = require('koa-router');
const koaBody = require('koa-body');

const AuthController = require('../controllers/auth-controller');
const StaticController = require('../controllers/static-controller');

const AuthMiddleware = require('../controllers/middlewares/auth-middleware')

const router = new Router({
  prefix: '/api',
});

router.post('/authorization', koaBody(), AuthController.SendSMS);
router.post('/verification', koaBody(), AuthController.Authorize);

router.get('/im', AuthController.GetAuth);
router.post('/register', koaBody(), AuthController.Register);

router.get('/countries', StaticController.GetCountries);
router.post('/countries', koaBody(), StaticController.AddCountry);

router.get('/middleware_check', AuthMiddleware, AuthController.MiddlewareCheck);

module.exports = router;
