const transform = (ctx, document) => {
    if (!document)
      return null
    
    return {
      id: document.id,
      name: document.name
    }
  };
  
  module.exports = transform;