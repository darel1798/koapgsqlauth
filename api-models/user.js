const country = require('./country');

const transform = (ctx, document) => {
    if (!document)
      return null
    
    return {
      id: document.id,
      name: document.name,
      email:document.email,
      phone: document.phone,
      city: document.city,
      country: country(ctx, document.Country),
      phoneOS: document.phoneOS,
      age: document.age
    }
  };
  
  module.exports = transform;