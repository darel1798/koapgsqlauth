
//Схема записи о верификации

module.exports = (sequelize,DataTypes) => {
    let Verification = sequelize.define('Verification',{
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
          },
          phone: {
            type: DataTypes.STRING,
            allowNull: false
          },
          key: {
            type: DataTypes.STRING,
            allowNull: false
          },
          code: {
            type: DataTypes.STRING,
            allowNull: false
          },
          actual: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
          },

    });

    return Verification;
}