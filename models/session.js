//Модель сессии

module.exports = (sequelize,DataTypes) => {
    let Session = sequelize.define('Session',{
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
          },
          token: {
            type: DataTypes.STRING,
            allowNull: false
          },
    });

    
    Session.associate = function(models) {
        Session.belongsTo(models.User, {});
      };

    return Session;
}