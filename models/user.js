
//Схема записи о пользователе

module.exports = (sequelize,DataTypes) => {
    let User = sequelize.define('User',{
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
          },
          name: {
            type: DataTypes.STRING,
            allowNull: false
          },
          email: {
            type: DataTypes.STRING,
            allowNull: false
          },
          phone: {
            type: DataTypes.STRING,
            allowNull: false
          },
          city: {
            type: DataTypes.STRING,
            allowNull: false
          },
          phoneOS: {
            type: DataTypes.STRING,
            allowNull: false
          },
          age: {
            type: DataTypes.INTEGER,
            allowNull: false
          }
    });

    User.associate = function(models) {
      User.belongsTo(models.Country,{
            onDelete : "SET NULL",
        });
    };

    return User;
}