
//Схема записи о стране

module.exports = (sequelize,DataTypes) => {
    let Country = sequelize.define('Country',{
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
          },
          name: {
            type: DataTypes.STRING,
            allowNull: false
          },
    });

    return Country;
}