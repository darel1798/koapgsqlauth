module.exports = {
  date: require('./date'),
  email: require('./email'),
  non_empty_string: require('./non-empty-string'),
  nonnegative_integer: require('./nonnegative-integer'),
  phone: require('./phone'),
};