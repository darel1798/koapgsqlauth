module.exports = (phone) => {
  return typeof phone === 'string' && phone.match(/^\d{11,13}$/);
};