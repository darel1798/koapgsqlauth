const emailValidator = require('email-validator');

module.exports = (value) => {
  return emailValidator.validate(value);
};
