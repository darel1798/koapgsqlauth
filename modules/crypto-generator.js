const crypto = require('crypto');
const util = require('util');

const randomBytes = util.promisify(crypto.randomBytes);

exports.generateKey = async () => {
  return (await randomBytes(32)).toString('hex')
};

exports.generateVerificationCode = async () => {
  return ((await randomBytes(4)).readUInt32LE() % 9000) + 1000;
};

exports.generateToken = async () => {
  return (await randomBytes(48)).toString('hex')
};