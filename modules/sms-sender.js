const config = require('../config/app.json');
const sms_ru = require('sms_ru');

const SMSRuInstance = new sms_ru(config.sms_ru_app_id);

exports.sendVerificationCode = (phone, code) => {
  SMSRuInstance.sms_send({
    to: phone,
    from: "Volunteer app",
    text: `Verification code: ${code}`
  }, () => {})
};

