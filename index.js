
const db = require('./models');
const Koa = require('koa');
const path = require('path')
const serve = require('koa-static')
const views = require('koa-views')
const config = require('./config/app.json')

const app = new Koa();

const mainRoutes = require('./routes/main-routes');

app.proxy = true

app.use(async (ctx, next) => {
    ctx.set('Access-Control-Allow-Origin', 'http://localhost:8080');
    ctx.set('Access-Control-Allow-Credentials', true);
    ctx.set('Access-Control-Allow-Methods', 'GET,PUT,POST,PATCH,DELETE,OPTIONS');
    ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Set-Cookie, *');

    if (ctx.method.toLowerCase() === 'options')
        return ctx.status = 200;

    return next()
})

//app.use(indexRouter.routes()).use(indexRouter.allowedMethods());
app.use(mainRoutes.routes()).use(mainRoutes.allowedMethods());

app.listen(config.port, (err) => {
    if (!err) {
        console.log((new Date()) + " Involvis app "+ config.dev + " " +config.port)
    }
});