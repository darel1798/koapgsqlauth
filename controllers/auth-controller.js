const moment = require('moment');
const CryptoGenerator = require('../modules/crypto-generator');
const { Op } = require('sequelize');
const SMS = require('../modules/sms-sender');
const Validators = require('../modules/validators')

const { Verification, User, Country, Session } = require('../models');

const config = require("../config/app.json");

const ApiModels = {
    user: require('../api-models/user')
}

exports.GetAuth = async (ctx) => {
    const token = ctx.request.header.token;

    if (!Validators.non_empty_string(token)) {
        ctx.body = {
            error: "Token not provided!"
        }
        return ctx.status = 401
    }

    const session = await Session.findOne({
        where: {
            token: token
        },
        include: [{
            model: User,
            as: 'User'
        }]
    })

    if (!session) {
        ctx.body = {
            error: "Invalid session"
        }
        return ctx.status = 401
    }

    //Сюда можно закинуть обновление токена при его "старении"

    return ctx.body = {
        token: session.token,
        user: ApiModels.user(ctx, session.User)
    }
}

exports.SendSMS = async (ctx) => {
    console.log("Sending sms-code")
    const phone = ctx.request.body.phone;

    if (!Validators.phone(phone)) {
        ctx.body = {
            error: "Invalid phone"
        }
        return ctx.status = 400
    }

    await Verification
        .update({
            actual: false
        }, {
            where: {
                actual: true,
                phone: phone
            },
            returning: false
        });

    const verification = await Verification.create({
        phone: phone,
        key: await CryptoGenerator.generateKey(),
        code: (Array.isArray(config.test_phone_range) && phone >= config.test_phone_range[0] && phone <= config.test_phone_range[1]) ? config.test_code : await CryptoGenerator.generateVerificationCode(),
        data: { phone: phone },
    })

    if (!(Array.isArray(config.test_phone_range) && phone >= config.test_phone_range[0] && phone <= config.test_phone_range[1]))
        SMS.sendVerificationCode(phone, verification.code);

    return ctx.body = {
        key: verification.key
    }
}

exports.Authorize = async (ctx) => {
    console.log("verification")
    const key = ctx.request.body.key, code = ctx.request.body.code;

    if (!Validators.non_empty_string(key)) {
        ctx.body = {
            error: "Invalid key"
        }
        return ctx.status = 400
    }
    if (!Validators.non_empty_string(code)) {

        ctx.body = {
            error: "Invalid code"
        }
        return ctx.status = 400
    }

    const acceptableDate = moment().subtract(5, 'm').toISOString();

    const verificationExists = await Verification.findOne({
        where: {
            actual: true,
            code,
            key,
            createdAt: {
                [Op.gte]: acceptableDate
            }
        }
    })

    if (!verificationExists) {
        ctx.body = {
            error: "Verification doesn't exists, or outdated"
        }
        return ctx.status = 400
    }

    verificationExists.actual = false
    verificationExists.save()

    let user = await User.findOne({
        where: { phone: verificationExists.phone },
        include: [{
            model: Country,
            as: 'Country'
        }],
    });

    if (!user) {
        ctx.body = {
            error: "User not found!"
        }
        return ctx.status = 400
    }

    let session = await Session.findOne({ where: { UserId: user.id } });

    if (!session) {
        let token;

        do {
            token = await CryptoGenerator.generateToken();
        }
        while (await Session.count({ where: { token: token } }));

        session = await Session.create({
            token: token,
            UserId: user.id
        })
    }


    ctx.body = {
        user: ApiModels.user(ctx, user),
        token: session.token,
    };
}

exports.Register = async (ctx) => {

    //Должен быть DeviceOS, его можно, как правило, вытащить из заголовка запроса
    const { name, email, phone, city, country, phoneOS, age } = ctx.request.body;

    console.log(!Validators.non_empty_string(name), !Validators.email(email), !Validators.phone(phone), !Validators.non_empty_string(city)
        , !Validators.nonnegative_integer(country), !Validators.non_empty_string(phoneOS), !Validators.nonnegative_integer(age))

    if (!Validators.non_empty_string(name) || !Validators.email(email) || !Validators.phone(phone) || !Validators.non_empty_string(city)
        || !Validators.nonnegative_integer(country) || !Validators.non_empty_string(phoneOS) || !Validators.nonnegative_integer(age)) {

        ctx.body = {
            error: "Invalid body"
        }
        return ctx.status = 400;
    }

    const countryExists = await Country.findByPk(country);

    if (!countryExists) {
        ctx.body = {
            error: "Invalid country"
        }
        return ctx.status = 400;
    }

    const userExists = await User.count({ where: { phone } });

    if (userExists > 0) {
        ctx.body = {
            error: "User already exists!"
        }
        return ctx.status = 400;
    }

    await User
        .create({
            name,
            email,
            phone,
            city,
            phoneOS,
            age,
            CountryId: country
        });

    return ctx.body = {
        success: true
    }
}

exports.MiddlewareCheck = async (ctx) => {
    return ctx.body = {
        success: true
    }
}