

const { Session } = require('../../models');
const Validators = require('../../modules/validators')

const moment = require('moment')

module.exports = async (ctx, next) => {

  const token = ctx.request.header.token;

  if(!Validators.non_empty_string(token)){
      ctx.body = {
          error: "Token not provided!"
      }
      return ctx.status = 401
  }

  const session = await Session.count({
      where: {
          token: token
      }
  })
  
  if(!session){
    ctx.body = {
      error: "Invalid session"
    }
    return ctx.status = 401
  }

  //Здесь должна быть обработка последней активности пользователя; заполнение контекста, к примеру, ролями пользователя;
 

  return next();
};