const moment = require('moment');
const CryptoGenerator = require('../modules/crypto-generator');
const SMS = require('../modules/sms-sender');
const Validators = require('../modules/validators')

const Country = require('../models').Country;

const config = require("../config/app.json");

const ApiModels = {
    country: require('../api-models/country')
}

exports.GetCountries = async(ctx) => {

    let countries = await Country.findAll({});


    return ctx.body = countries.map(doc => ApiModels.country(ctx,doc));
}

exports.AddCountry = async(ctx) => {

    const {name} = ctx.request.body;

    if(!Validators.non_empty_string(name)){
        ctx.body = {
            err: "Invalid body"
        }
        return ctx.status = 400;
    }

    const country = await Country
                .create({
                    name: name,
                });
                
    return ctx.body = ApiModels.country(ctx, country)
}